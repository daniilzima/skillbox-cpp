
#include <iostream>


const int N = 21;
// If a == true only even numbers are shown, else only uneven numbers are shown
bool a = true;

void print()
{
    if (a){
        for (int start = 1; start != (N + 1); start++)
        {
            if (start % 2 == 0) {
                std::cout << start << '\n';
            }
        }
    }
    else {
        for (int start = 1; start != (N + 1); start++)
        {
            if (start % 2 != 0) {
                std::cout << start << '\n';
            }
        }
    }
}

int main()
{
    print();
}

