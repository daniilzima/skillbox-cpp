#define _CRT_SECURE_NO_WARNINGS
#include <vector>
#include <iostream>
#include <time.h>


void Printmatrix(const std::vector<std::vector<int>>& matrix)
{
	for (const auto row : matrix)
	{
		for (const auto collumn : row)
		{
			std::cout << collumn << " ";
		}
		std::cout << std::endl;
	}
}

void fillMatrix(std::vector<std::vector<int>>& Matrix, int SizeMatrix)
{
	Matrix.resize(SizeMatrix);
	for (int i = 0; i < SizeMatrix; i++)
	{
		Matrix[i].resize(SizeMatrix);
		for (int j = 0; j < SizeMatrix; j++)
		{
			Matrix[i][j] = i + j;
		}
	}
}

void PrintRowsSum(const std::vector<std::vector<int>>& Matrix, int SizeMatrix)
{
	// for date
	struct tm* tim;
	time_t tt = time(NULL);
	tim = localtime(&tt);
	std::cout << "\nCurrent day - " << tim->tm_mday << "\n";
	//


	std::cout << "Rows Sum: \n";
	for (int i = 0; i < Matrix.size(); i++)
	{
		if (i == tim->tm_mday % SizeMatrix)
		{
			int TempSum = 0;
			for (int j = 0; j < Matrix[i].size(); j++)
			{
				TempSum += j;
			}
			std::cout << "Row " << i << " = " << TempSum << std::endl;
		}
	}
}

int main()
{
	std::vector<std::vector<int>> Matrix;
	int SizeMatrix;

	std::cout << "Enter rows count: ";
	std::cin >> SizeMatrix;

	fillMatrix(Matrix, SizeMatrix);

	Printmatrix(Matrix);

	PrintRowsSum(Matrix, SizeMatrix);

	return 0;
}